# Topological Fingerprint for Time-series
### This work was presented in Toyota Research Institute AMDD Conference 2019, bottom half of the poster below.
![](TimeSeries_Poster_finalV2_bottom.png)
Data is taken from https://www.nature.com/articles/s41560-019-0356-8

### Main Ideas  
- battery test data contain cycle of different measurements, e.g., C, V, Q...
- instead of looking at how the different measures change over time individually, we look at how the changes they have differ from one other.
- using zigzag persistence and clustering to track the change (with the help of correlation coefficients)
- barcodes provide a quick visual check as well as metric for comparison
